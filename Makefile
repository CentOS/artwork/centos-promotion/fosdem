all:

Final/%.png: Sources/%.svg
	inkscape --export-filename=$@ $<
	pngquant -f --ext .png $@
