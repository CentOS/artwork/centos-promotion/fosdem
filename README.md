# Fosdem

This project provides images to promote the [CentOS
Project](https://www.centos.org/) at [FOSDEM](https://fosdem.org/) events. 

## Website

![CentOS Connect](Final/centos-connect-website-c10.png)

![CentOS Connect](Final/centos-connect-website-banner-bg-c10.png)

## Presentation slides

| Title | Content |
| ----- | ------- |
| ![Title](remark/img/screenshot-title.png) | ![Content](remark/img/screenshot-content.png) |

To create your presentation:
1. Copy the [./remark](./remark/) directory to your workstation.
1. Edit the `index.html` file to write your presentation using markdown.
1. Open the `index.html` file in a browser to present your slides.

See [remark documentation](https://github.com/gnab/remark/wiki/Markdown) for further customization features.

## Social media

Reusable on vinyl banner.

### Without artistic motif

![CentOS Connect](Final/centos-connect.png)

### With artistic motif

![CentOS Connect](Final/centos-connect-c10.png)
![CentOS Connect](Final/centos-connect-c10-alternative-1.png)
![CentOS Connect](Final/centos-connect-c10-alternative-2.png)


## License

The CentOS promotional artwork is released under the terms of [Creative Commons
Attribution-ShareAlike 4.0 International Public
License](https://creativecommons.org/licenses/by-sa/4.0/legalcode), and usage
limited by the [CentOS Trademark
Guidelines](https://www.centos.org/legal/trademarks/).
